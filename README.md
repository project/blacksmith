The goal of this module is provide a flexible and robust way to package your site's content.
By using Blacksmith your administrators will be able to import and export packages of entities.

## Development Roadmap

### 8.x.1.x : Alpha 1
* ~~Proof of concept~~

### 8.x.1.x : Alpha 2
* ~~Officially rename the module to Blacksmith~~
* ~~Code stabilization for PHP 7.3~~

### 8.x.1.x : Alpha 3
* ~~Proper dependency injection for FieldFormatters~~
* Explore the necessity of DI in EntityImporter
* Menu item support
* Files and Medias support
* Blacksmith file dependencies system
* Multilingual support

### 8.x.1.x : Alpha 4
* Improve error handling
* Dedicated exceptions indicating why an error occurs and which item is causing it.
* Distinguish errors that should skip an item VS errors that should stop everything.
* Improve Drush commands and tools to run Blacksmith

### 8.x.1.x : Alpha 5
* Rollback mechanic (similar to Migrate)
* Import and update content instead of create new items
* Use Annotations and plugins to make the module extendable by other contrib modules.
* Reference to content created by another Blacksmith file

## Ideas
* Generate Blacksmith file based on bundle (use `drush generate)
* Blacksmith UI submodule
* Random content generator
  * Repeat item creation N times
