<?php

namespace Drupal\blacksmith\Blacksmith;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\blacksmith\Exception\BlacksmithImportBreak;
use Drupal\blacksmith\Exception\BlacksmithImportSkip;
use Drupal\blacksmith\Blacksmith\EntityImporter\EntityImporterFactory;
use Drupal\blacksmith\BlacksmithGroup;
use Drupal\blacksmith\BlacksmithItem;

/**
 * Class ContentImporter.
 */
class ContentImporter {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Blacksmith entity importer factory service.
   *
   * @var \Drupal\blacksmith\Blacksmith\EntityImporter\EntityImporterFactory
   */
  protected $entityImporterFactory;

  /**
   * BlacksmithContentImporter constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger factory instance.
   * @param \Drupal\blacksmith\Blacksmith\EntityImporter\EntityImporterFactory $entityImporterFactory
   *   Blacksmith entity importer factory service.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory, EntityImporterFactory $entityImporterFactory) {
    $this->logger = $loggerFactory->get('blacksmith');
    $this->entityImporterFactory = $entityImporterFactory;
  }

  /**
   * Imports all given groups.
   *
   * @param \Drupal\blacksmith\BlacksmithGroup $group
   *   Blacksmith group we are about to import.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function importGroup(BlacksmithGroup $group) : void {
    $items = $group->getItems();

    foreach ($items as $item) {
      try {
        $this->importItem($item);
      }
      catch (BlacksmithImportSkip $exception) {
        continue;
      }
      catch (BlacksmithImportBreak $exception) {
        break;
      }
    }
  }

  /**
   * Import a specific Blacksmith item.
   *
   * @param \Drupal\blacksmith\BlacksmithItem $item
   *   Blacksmith item.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Created entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\blacksmith\Exception\BlacksmithImportSkip
   * @throws \Drupal\blacksmith\Exception\BlacksmithImportBreak
   */
  public function importItem(BlacksmithItem $item) : EntityInterface {
    $importer = $this->entityImporterFactory->create($item->entityType());
    return $importer->import($item);
  }

}
