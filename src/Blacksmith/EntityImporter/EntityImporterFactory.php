<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\blacksmith\Exception\BlacksmithImportSkip;

/**
 * Class EntityImporterFactory.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter
 */
final class EntityImporterFactory {

  /**
   * EntityImporter instances.
   *
   * @var \Drupal\blacksmith\Blacksmith\EntityImporter\EntityImporterInterface[]
   */
  protected $importers;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * EntityImporterFactory constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Retrieves the correct EntityImporter based on the item's configurations.
   *
   * @param string $entityType
   *   The type of entity you want to create.
   *
   * @return \Drupal\blacksmith\Blacksmith\EntityImporter\EntityImporterInterface
   *   The correct entity type importer for the item.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithImportSkip
   */
  public function create($entityType) : EntityImporterInterface {

    try {
      switch ($entityType) {
        case 'file':
          if (!isset($this->importers['file'])) {
            $storage = $this->entityTypeManager->getStorage('file');
            $this->importers['file'] = new FileEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['file'];

        case 'media':
          if (!isset($this->importers['media'])) {
            $storage = $this->entityTypeManager->getStorage('media');
            $this->importers['media'] = new MediaEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['media'];

        case 'node':
          if (!isset($this->importers['node'])) {
            $storage = $this->entityTypeManager->getStorage('node');
            $this->importers['node'] = new NodeEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['node'];

        case 'paragraph':
          if (!isset($this->importers['paragraph'])) {
            $storage = $this->entityTypeManager->getStorage('paragraph');
            $this->importers['paragraph'] = new ParagraphEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['paragraph'];

        case 'term':
        case 'taxonomy_term':
          if (!isset($this->importers['taxonomy_term'])) {
            $storage = $this->entityTypeManager->getStorage('taxonomy_term');
            $this->importers['taxonomy_term'] = new TaxonomyTermEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['taxonomy_term'];

        case 'user':
          if (!isset($this->importers['user'])) {
            $storage = $this->entityTypeManager->getStorage('user');
            $this->importers['user'] = new UserEntityImporter($storage, $this->entityTypeManager, $this->entityFieldManager, clone $this);
          }

          return $this->importers['user'];

        default:
          throw new BlacksmithImportSkip('Blacksmith : Could not find an EntityImporter to importer content of type : ' . $entityType);
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $exception) {
      throw new BlacksmithImportSkip('Could not find an entity storage for content of type : ' . $entityType);
    }

  }

}
