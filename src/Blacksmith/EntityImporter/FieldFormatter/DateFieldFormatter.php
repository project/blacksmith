<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal;

/**
 * Class DateFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class DateFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Datetime\DateFormatter $dateFormatter
   */
  protected function formatUniqueValue($value) {
    $dateFormatter = Drupal::service('date.formatter');
    $type = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('datetime_type');
    $value = strtotime($value);

    switch ($type) {
      case 'datetime':
        $value = $dateFormatter->format($value, 'custom', "Y-m-d\TH:i:s");
        break;

      case 'date':
        $value = $dateFormatter->format($value, 'custom', 'Y-m-d');
        break;

      case 'daterange':
        $value = [
          'value' => $value,
          'end_value' => $value,
        ];
    }

    return $value;
  }

}
