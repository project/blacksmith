<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal\field\Entity\FieldConfig;

/**
 * Class NumberFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class NumberFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function validateUniqueValue($value) : bool {
    if (!parent::validateUniqueValue($value)) {
      return FALSE;
    }

    if (is_numeric($value) === FALSE) {
      return FALSE;
    }

    // Make sure that the value has the correct type.
    $value = (float) $value;

    // Validate that the number fits in the range.
    if ($this->fieldDefinition instanceof FieldConfig && $settings = $this->fieldDefinition->getSettings()) {
      if (isset($settings['min']) && $value < (float) $settings['min']) {
        return FALSE;
      }

      if (isset($settings['max']) && $value > (float) $settings['max']) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
