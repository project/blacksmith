<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class BooleanFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class BooleanFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function formatUniqueValue($value) {
    return (bool) $value ? 1 : 0;
  }

}
