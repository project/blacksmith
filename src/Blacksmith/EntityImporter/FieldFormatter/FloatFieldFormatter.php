<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class FloatFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class FloatFieldFormatter extends NumberFieldFormatter {

  /**
   * {@inheritdoc}
   */
  protected function validateUniqueValue($value) : bool {
    if (!parent::validateUniqueValue($value)) {
      return FALSE;
    }

    return is_float($value);
  }

}
