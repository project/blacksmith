<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class EntityReferenceRevisionsFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class EntityReferenceRevisionsFieldFormatter extends EntityReferenceFieldFormatter {

  /**
   * The way the entity is referenced in the database.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The sub entity that was just created.
   *
   * @return mixed
   *   The way the entity is saved the database.
   */
  protected function formatEntity(EntityInterface $entity) {
    if ($entity instanceof ContentEntityBase) {
      return [
        'target_id' => $entity->id(),
        'target_revision_id' => $entity->getRevisionId(),
      ];
    }

    return parent::formatEntity($entity);
  }

}
