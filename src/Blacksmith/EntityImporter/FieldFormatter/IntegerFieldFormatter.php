<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class IntegerFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class IntegerFieldFormatter extends NumberFieldFormatter {

  /**
   * {@inheritdoc}
   */
  protected function validateUniqueValue($value) : bool {
    if (!parent::validateUniqueValue($value)) {
      return FALSE;
    }

    return is_int($value);
  }

}
