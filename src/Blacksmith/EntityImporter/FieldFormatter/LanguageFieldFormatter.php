<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LanguageFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class LanguageFieldFormatter extends FieldFormatterBase implements ContainerInjectionInterface {

  /**
   * Drupal's language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * LanguageFieldFormatter constructor.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The definition of the field being imported.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Drupal's language manager service.
   */
  public function __construct(FieldDefinitionInterface $fieldDefinition, LanguageManagerInterface $languageManager) {
    parent::__construct($fieldDefinition);
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, FieldDefinitionInterface $fieldDefinition = NULL) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $fieldDefinition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function validateUniqueValue($value) : bool {
    $language = $this->languageManager->getLanguage($value);

    if ($language === NULL) {
      return FALSE;
    }

    return parent::validateUniqueValue($value);
  }

}
