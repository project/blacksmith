<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Class FieldFormatterFactory.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class FieldFormatterFactory {

  /**
   * Service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * FieldFormatterFactory constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->container = Drupal::getContainer();
  }

  /**
   * Defines the correct FieldFormatter for a specific field type.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition of the field about to be imported.
   *
   * @return \Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter\FieldFormatterInterface
   *   The correct FieldFormatted best suited to format to field.
   */
  public function create(FieldDefinitionInterface $fieldDefinition) : FieldFormatterInterface {
    $fieldName = $fieldDefinition->getName();
    $fieldType = $fieldDefinition->getType();

    switch ($fieldType) {
      case 'boolean':
        $class = BooleanFieldFormatter::class;
        break;

      case 'created':
      case 'changed':
      case 'datetime':
      case 'timestamp':
        $class = DateFieldFormatter::class;
        break;

      case 'daterange':
        $class = DateRangeFieldFormatter::class;
        break;

      case 'email':
      case 'list_string':
      case 'string':
      case 'string_long':
      case 'link':
      case 'telephone':
        $class = FieldFormatterBase::class;
        break;

      case 'text':
      case 'text_long':
        $class = FormattedTextFieldFormatter::class;
        break;

      case 'text_with_summary':
        $class = FormattedTextWithSummaryFieldFormatter::class;
        break;

      case 'language':
        $class = LanguageFieldFormatter::class;
        break;

      case 'integer':
      case 'list_integer':
        $class = IntegerFieldFormatter::class;
        break;

      case 'float':
      case 'decimal':
      case 'list_float':
        $class = FloatFieldFormatter::class;
        break;

      case 'file':
        $class = FileFieldFormatter::class;
        break;

      case 'image':
        $class = ImageFieldFormatter::class;
        break;

      case 'entity_reference':
        $class = EntityReferenceFieldFormatter::class;
        break;

      case 'entity_reference_revisions':
        $class = EntityReferenceRevisionsFieldFormatter::class;
        break;

      default:
        Drupal::messenger()->addWarning("Missing FieldFormatter for field : $fieldName ($fieldType)");
        $class = FieldFormatterBase::class;
        break;
    }

    // Check if the FieldFormatter requires any Drupal services.
    // @todo Probably not the right way to do this.
    if (in_array(ContainerInjectionInterface::class, class_implements($class), TRUE)) {
      /* @noinspection PhpUndefinedMethodInspection */
      return $class::create($this->container, $fieldDefinition);
    }

    return new $class($fieldDefinition);
  }

}
