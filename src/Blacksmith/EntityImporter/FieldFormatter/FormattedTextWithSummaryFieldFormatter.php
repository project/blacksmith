<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class FormattedTextWithSummaryFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class FormattedTextWithSummaryFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function formatUniqueValue($value) : array {

    if (is_string($value)) {
      return [
        'value' => $value,
        'format' => NULL,
        'summary' => '',
      ];
    }

    return $value;
  }

}
