<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Interface FieldImporterInterface.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
interface FieldFormatterInterface {

  /**
   * Edit the value from the Yaml file and transform it to save in the database.
   *
   * @param array|string $values
   *   Value from the Blacksmith file.
   *
   * @return mixed
   *   The value to save in the database.
   */
  public function format($values);

}
