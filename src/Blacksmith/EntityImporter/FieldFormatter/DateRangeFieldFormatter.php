<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class DateFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class DateRangeFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Datetime\DateFormatter $dateFormatter
   */
  protected function formatUniqueValue($value) {
    $dateFormatter = \Drupal::service('date.formatter');
    $value = $dateFormatter->format(strtotime($value), 'custom', "Y-m-d\TH:i:s");

    $value = [
      'value' => $value,
      'end_value' => $value,
    ];

    return $value;
  }

}
