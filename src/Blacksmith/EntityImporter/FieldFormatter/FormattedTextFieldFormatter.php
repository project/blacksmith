<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class FormattedTextFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class FormattedTextFieldFormatter extends FieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function formatUniqueValue($value) : array {

    // Make sure that the value is always provided as an array.
    if (is_string($value)) {
      return [
        'value' => $value,
        'format' => NULL,
      ];
    }

    return $value;
  }

}
