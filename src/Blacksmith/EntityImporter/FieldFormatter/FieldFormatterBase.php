<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Class FieldFormatterBase.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class FieldFormatterBase implements FieldFormatterInterface {
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * The field definition of the field about to be imported.
   *
   * @var \Drupal\Core\Field\FieldDefinition
   */
  protected $fieldDefinition;

  /**
   * The module's logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * FieldFormatterBase constructor.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition of the field about to be imported.
   */
  public function __construct(FieldDefinitionInterface $fieldDefinition) {
    $this->fieldDefinition = $fieldDefinition;
    $this->logger = $this->getLogger('blacksmith');
  }

  /**
   * Prepare all values to be saved in the database.
   *
   * @param array|string $values
   *   A value from the Blacksmith file.
   *
   * @return mixed
   *   The value ready to be saved in the database.
   */
  public function format($values) {
    $fieldName = $this->fieldDefinition->getName();

    if (!$this->hasMultiple($values)) {
      $values = [$values];
    }
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    if ($cardinality !== -1 && count($values) > $cardinality) {
      $this->messenger()->addWarning("The field '$fieldName' only supports up to $cardinality value(s).");
    }

    $formattedValues = [];
    foreach ($values as $value) {
      if (!$this->validateUniqueValue($value)) {
        $this->messenger()->addWarning("Invalid value : '$value' in field '$fieldName'");
        continue;
      }

      $value = $this->formatUniqueValue($value);
      $formattedValues[] = $value;
    }

    return $formattedValues;
  }

  /**
   * Prepare an individual value to be saved in the database.
   *
   * @param mixed $value
   *   A unique value from the Blacksmith file.
   *
   * @return mixed
   *   The value to save in the database.
   */
  protected function formatUniqueValue($value) {
    return $value;
  }

  /**
   * Checks if the value actually has multiple values.
   *
   * @param mixed $value
   *   The value from the Blacksmith file.
   *
   * @return bool
   *   Whether or not there are multiple values.
   */
  protected function hasMultiple($value) : bool {
    if (!is_array($value)) {
      return FALSE;
    }

    if ($value === []) {
      return FALSE;
    }

    return array_keys($value) === range(0, count($value) - 1);
  }

  /**
   * Make sure that the value is valid if based on the field restrictions.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @return bool
   *   Whether the value is valid or not.
   */
  protected function validateUniqueValue($value) : bool {
    if ($allowedValues = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('allowed_values')) {
      return array_key_exists($value, $allowedValues);
    }

    return TRUE;
  }

}
