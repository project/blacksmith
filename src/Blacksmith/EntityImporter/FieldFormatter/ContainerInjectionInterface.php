<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface as ContainerInjectionInterfaceBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface ContainerInjectionInterface.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
interface ContainerInjectionInterface extends ContainerInjectionInterfaceBase {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, FieldDefinitionInterface $fieldDefinition = NULL);

}
