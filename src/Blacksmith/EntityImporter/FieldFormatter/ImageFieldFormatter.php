<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter;

/**
 * Class ImageFieldFormatter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter\FieldFormatter
 */
class ImageFieldFormatter extends FileFieldFormatter {

  /**
   * {@inheritdoc}
   */
  protected function formatUniqueValue($value) : array {
    $originalValue = $value;
    $value = parent::formatUniqueValue($value);

    if (isset($originalValue['alt'])) {
      $value['alt'] = $originalValue['alt'];
    }

    return $value;
  }

}
