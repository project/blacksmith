<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter;

use Drupal\blacksmith\BlacksmithItem;

/**
 * Class MediaEntityImporter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter
 */
class MediaEntityImporter extends EntityImporter {

  /**
   * {@inheritdoc}
   */
  protected function getFieldDefinitions(BlacksmithItem $item) : array {
    $fieldDefinitions = parent::getFieldDefinitions($item);
    unset($fieldDefinitions['bundle']);

    return $fieldDefinitions;
  }

}
