<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\blacksmith\BlacksmithItem;

/**
 * Interface EntityImporterInterface.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter
 */
interface EntityImporterInterface {

  /**
   * Creates a new entity based on the Blacksmith item.
   *
   * @param \Drupal\blacksmith\BlacksmithItem $item
   *   Blacksmith item used to build the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The created entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\blacksmith\Exception\BlacksmithImportSkip
   * @throws \Drupal\blacksmith\Exception\BlacksmithImportBreak
   */
  public function import(BlacksmithItem $item) : EntityInterface;

}
