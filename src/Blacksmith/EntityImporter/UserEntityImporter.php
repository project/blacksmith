<?php

namespace Drupal\blacksmith\Blacksmith\EntityImporter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\blacksmith\Exception\BlacksmithImportSkip;
use Drupal\blacksmith\BlacksmithItem;

/**
 * Class UserEntityImporter.
 *
 * @package Drupal\blacksmith\Blacksmith\EntityImporter
 */
final class UserEntityImporter extends EntityImporter {

  /**
   * {@inheritdoc}
   */
  protected function presetValues(BlacksmithItem $item) : array {
    return [
      'name' => $item->label(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function createEntity(array $values) : EntityInterface {
    $users = $this->entityStorage->loadByProperties([
      'name' => $values['name'],
    ]);

    // @todo Should we throw an error or update the existing entity?
    if (!empty($users)) {
      throw new BlacksmithImportSkip('The user ' . $values['name'] . ' already exists');
    }

    return parent::createEntity($values);
  }

}
