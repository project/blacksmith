<?php

namespace Drupal\blacksmith\Blacksmith;

use Drupal\Component\Discovery\DiscoveryException;
use Drupal\Component\Discovery\YamlDirectoryDiscovery;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\blacksmith\BlacksmithGroup;

/**
 * Class ContentDiscovery.
 */
class ContentDiscovery extends YamlDirectoryDiscovery {

  /**
   * Blacksmith's Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Module's configurations.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $configs;

  /**
   * ContentDiscovery constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger factory instance.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory instance.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory, ConfigFactory $configFactory) {
    $this->configs = $configFactory->get('blacksmith.settings');
    $this->logger = $loggerFactory->get('blacksmith');
    parent::__construct([$this->getSourceDirectory()], 'blacksmith', 'id');
  }

  /**
   * Retrieve the configured Blacksmith source directory.
   *
   * @return string
   *   The directory containing the source data files.
   */
  public function getSourceDirectory() : string {
    return $this->configs->get('source.path');
  }

  /**
   * Get all the content available in the Blacksmith source files.
   *
   * @todo Reorder the values based on the dependencies.
   *
   * @return \Drupal\blacksmith\BlacksmithGroup[]
   *   List of blacksmith content items.
   *
   * @throws \Exception
   */
  public function findAllGroups() : array {
    return $this->getGroups();
  }

  /**
   * Get specific Blacksmith groups.
   *
   * @param array $groupIds
   *   List of Blacksmith group IDs.
   *
   * @return \Drupal\blacksmith\BlacksmithGroup[]
   *   List of blacksmith content items.
   *
   * @throws \Exception
   */
  public function getGroups(array $groupIds = []) : array {
    $groups = [];
    $all = $this->findAll();

    foreach ($all as $provider => $files) {
      foreach ($files as $groupId => $data) {

        // Check if the file has the required properties.
        $this->validateFileProperties($groupId, $data);
        if (empty($groupIds) || in_array($groupId, $groupIds)) {
          $groups[$groupId] = new BlacksmithGroup($data);
        }
      }
    }

    return $groups;
  }

  /**
   * Makes sure that the Yml file has the necessary information.
   *
   * @param string $identifier
   *   Blacksmith file identifier.
   * @param array $data
   *   The data from the YAML file.
   *
   * @throws \Drupal\Component\Discovery\DiscoveryException
   */
  protected function validateFileProperties($identifier, array $data) : void {
    if (!isset($data['label'])) {
      throw new DiscoveryException("Blacksmith file '$identifier' is missing the 'label' property.");
    }

    if (!isset($data['items'])) {
      throw new DiscoveryException("Blacksmith file '$identifier' is missing the 'items' property.");
    }

    if (isset($data['default_bundle']) && !isset($data['default_entity_type'])) {
      throw new DiscoveryException("Blacksmith : Cannot define the 'default_bundle' without the 'default_entity_type' property in the '$identifier' group.");
    }

  }

}
