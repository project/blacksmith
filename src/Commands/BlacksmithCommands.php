<?php

namespace Drupal\blacksmith\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\blacksmith\Blacksmith\ContentDiscovery;
use Drupal\blacksmith\Blacksmith\ContentImporter;
use Drush\Commands\DrushCommands;
use Exception;

/**
 * Class BlacksmithCommands.
 *
 * @noinspection PhpUnused
 *
 * @package Drupal\blacksmith\Commands
 */
class BlacksmithCommands extends DrushCommands {

  /**
   * Blacksmith content discovery service.
   *
   * @var \Drupal\blacksmith\Blacksmith\ContentDiscovery
   */
  protected $contentDiscoverer;

  /**
   * Blacksmith content importer service.
   *
   * @var \Drupal\blacksmith\Blacksmith\ContentImporter
   */
  protected $contentImporter;

  /**
   * BlacksmithCommands constructor.
   *
   * @param \Drupal\blacksmith\Blacksmith\ContentDiscovery $contentDiscoveryService
   *   Blacksmith content discovery service.
   * @param \Drupal\blacksmith\Blacksmith\ContentImporter $contentImporter
   *   Blacksmith content importer service.
   */
  public function __construct(ContentDiscovery $contentDiscoveryService, ContentImporter $contentImporter) {
    parent::__construct();
    $this->contentDiscoverer = $contentDiscoveryService;
    $this->contentImporter = $contentImporter;
  }

  /**
   * Provides the list of content to import and content already imported.
   *
   * @command blacksmith:status
   * @aliases blacksmith-status, bss
   * @field-labels
   *   name: Blacksmith Name
   *   entity_type: Default entity type
   *   bundle: Default bundle
   *   total: Total
   * @default-fields name,entity_type,bundle,total
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Formatted table.
   *
   * @throws \Exception
   * @noinspection PhpUnused
   */
  public function statusCommand() : RowsOfFields {
    $groups = $this->contentDiscoverer->findAllGroups();
    $table = [];

    foreach ($groups as $group) {
      $table[] = [
        'name' => $group->fullLabel(),
        'total' => count($group->getItems()),
        'entity_type' => $group->getDefault('entity_type'),
        'bundle' => $group->getDefault('bundle'),
      ];
    }

    return new RowsOfFields($table);
  }

  /**
   * Generate the content based on the source data files.
   *
   * @param string $groupNames
   *   Restrict to a comma-separated list of Blacksmith groups (Optional).
   *
   * @command blacksmith:import
   * @aliases blacksmith-import, bsi
   * @option group A comma-separated list of migration groups to list
   *
   * @default $options []
   *
   * @throws \Exception
   * @noinspection PhpUnused
   */
  public function importCommand($groupNames = '') : void {

    if ($groupNames) {
      $groups = explode(',', mb_strtolower($groupNames));
      $groups = $this->contentDiscoverer->getGroups($groups);
    }
    else {
      $groups = $this->contentDiscoverer->findAllGroups();
    }

    foreach ($groups as $group) {
      $groupLabel = $group->fullLabel();
      $this->io()->write("Importing Blacksmith group : $groupLabel", FALSE);

      try {
        $this->contentImporter->importGroup($group);
        $this->inlineSuccess(' Success!');
      }
      catch (Exception $exception) {
        $this->inlineError(' ERROR!');
        $this->io()->error($exception->getMessage());
      }
    }
  }

  /**
   * Utility method to print a message in green.
   *
   * @param string $message
   *   The message we want to display in green.
   */
  protected function inlineSuccess($message) : void {
    $this->io()->write("\033[0;32m$message\033[0m", FALSE);
    $this->io()->writeln('');
  }

  /**
   * Utility method to print a message in red.
   *
   * @param string $message
   *   The message we want to display in red.
   */
  protected function inlineError($message) : void {
    $this->io()->write("\033[0;31m$message\033[0m", FALSE);
    $this->io()->writeln('');
  }

}
