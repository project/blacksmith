<?php

namespace Drupal\blacksmith\Exception;

use Exception;

/**
 * Class BlacksmithException.
 *
 * @package Drupal\blacksmith\Exception
 */
class BlacksmithException extends Exception {

}
