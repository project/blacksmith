<?php

namespace Drupal\blacksmith\Exception;

use Drupal\blacksmith\BlacksmithItem;

/**
 * Class BlacksmithImportException.
 *
 * @package Drupal\blacksmith\Exceptions
 */
class BlacksmithImportException extends BlacksmithException {

  /**
   * Cause of the import fail.
   *
   * @var \Drupal\blacksmith\BlacksmithItem
   */
  protected $item;

  /**
   * BlacksmithImportException constructor.
   *
   * @param string $message
   *   Description of the error.
   * @param \Drupal\blacksmith\BlacksmithItem $item
   *   Blacksmith item that caused the issue.
   */
  public function __construct($message, BlacksmithItem $item = NULL) {
    $this->item = $item;
    $selector = $item->selector();
    $message .= " ($selector)";

    parent::__construct($message);
  }

  /**
   * Returns the Blacksmith item that caused the issue.
   *
   * @return \Drupal\blacksmith\BlacksmithItem
   *   Blacksmith item that caused the issue.
   */
  public function getItem() : BlacksmithItem {
    return $this->item;
  }

}
