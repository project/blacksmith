<?php

namespace Drupal\blacksmith\Exception;

/**
 * Class BlacksmithImportSkip.
 *
 * @package Drupal\blacksmith\Exception
 */
class BlacksmithImportSkip extends BlacksmithImportException {

}
