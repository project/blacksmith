<?php

namespace Drupal\blacksmith\Exception;

/**
 * Class BlacksmithImportBreak.
 *
 * @package Drupal\blacksmith\Exception
 */
class BlacksmithImportBreak extends BlacksmithException {

}
