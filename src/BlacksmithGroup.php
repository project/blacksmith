<?php

namespace Drupal\blacksmith;

use Drupal\blacksmith\Exception\BlacksmithException;

/**
 * Class BlacksmithGroup.
 *
 * @package Drupal\blacksmith
 */
class BlacksmithGroup {

  /**
   * The group ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The group's label.
   *
   * @var string
   */
  protected $label;

  /**
   * Original data from the YAML file.
   *
   * @var array
   */
  protected $originalData;

  /**
   * The list of items in this group.
   *
   * @var \Drupal\blacksmith\BlacksmithItem[]
   */
  protected $items;

  /**
   * Default langcode.
   *
   * @var string|null
   */
  protected $defaultValues;

  /**
   * The Blacksmith groups that needs to be imported before end.
   *
   * @var array
   */
  protected $dependencies;

  /**
   * BlacksmithGroup constructor.
   *
   * @param array $data
   *   Data from the Blacksmith source file.
   *
   * @throws \Exception
   */
  public function __construct(array $data) {
    $this->validateData($data);
    $this->id = $data['id'];
    $this->originalData = $data;
    $this->label = $data['label'];
    $this->defaultValues = $data['default'] ?? [];
    $this->setItems($data['items']);

    if (isset($data['dependencies'])) {
      $this->dependencies = is_array($data['dependencies']) ? $data['dependencies'] : [$data['dependencies']];
    }
  }

  /**
   * Makes sure that the parameters given to the constructor are valid.
   *
   * @param array $data
   *   Data from the YAML file.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithException
   */
  protected function validateData(array $data) : void {
    if (!isset($data['id'])) {
      throw new BlacksmithException('Missing parameter \'ID\' in Blacksmith group.');
    }

    if (!isset($data['label'])) {
      throw new BlacksmithException('Missing parameter \'label\' in Blacksmith group.');
    }

    if (!isset($data['items'])) {
      throw new BlacksmithException('"Missing parameter \'items\' in Blacksmith group.');
    }

    if (empty($data['items'])) {
      throw new BlacksmithException('A Blacksmith group must have at least 1 item.');
    }

    if (isset($data['default']['bundle']) && !isset($data['default']['entity_type'])) {
      throw new BlacksmithException('Cannot define the \'default_bundle\' without the \'default_entity_type\' parameter.');
    }
  }

  /**
   * Get the group's ID.
   *
   * @return string
   *   Item's group name.
   */
  public function id() : string {
    return $this->id;
  }

  /**
   * Get the group's label.
   *
   * @return string
   *   The group's label.
   */
  public function label() : string {
    return $this->label;
  }

  /**
   * Get the group's label including the ID.
   *
   * @return string
   *   Group label and id.
   */
  public function fullLabel() : string {
    return $this->label . ' (' . $this->id . ')';
  }

  /**
   * Get the list of item in this field group.
   *
   * @return \Drupal\blacksmith\BlacksmithItem[]
   *   The list of Blacksmith items in this group.
   */
  public function getItems() : array {
    return $this->items ?? [];
  }

  /**
   * Gives access to the default values.
   *
   * @param string $key
   *   The key of the default value.
   *
   * @return string
   *   The default value or NULL
   */
  public function getDefault($key) : ?string {
    return $this->defaultValues[$key] ?? NULL;
  }

  /**
   * Sets all items in the Blacksmith group.
   *
   * @param array $itemsData
   *   Items data from the YAML file.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithException
   */
  protected function setItems(array $itemsData) : void {
    $ids = [];

    foreach ($itemsData as $itemData) {
      if (!is_array($itemData)) {
        continue;
      }

      /* @noinspection SlowArrayOperationsInLoopInspection */
      $itemData = array_merge($this->defaultValues, $itemData);
      $item = new BlacksmithItem($itemData, $this->id);

      if (in_array($item->id(), $ids, FALSE)) {
        throw new BlacksmithException("Duplicate ID '" . $item->id() . "' found at index " . count($ids) . " in the '" . $this->id . "' group.");
      }

      $this->items[] = $item;
      $ids[] = $item->id();
    }
  }

}
