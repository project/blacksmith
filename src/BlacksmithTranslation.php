<?php

namespace Drupal\blacksmith;

/**
 * Class BlacksmithTranslation.
 *
 * @package Drupal\blacksmith
 */
class BlacksmithTranslation {

  /**
   * Blacksmith translation language.
   *
   * @var string
   */
  protected $langcode;

  /**
   * Data taken from the Yaml file.
   *
   * @var array
   */
  protected $originalData;

  /**
   * BlacksmithItem constructor.
   *
   * @param string $langcode
   *   The language of this Blacksmith Item translation.
   * @param array $data
   *   Data from the Blacksmith source file.
   */
  public function __construct($langcode, array $data) {
    $this->langcode = $langcode;
    $this->originalData = $data;
  }

  /**
   * Get the translation's label.
   *
   * @return string|null
   *   The bundle machine name.
   */
  public function label() : ?string {
    $labelKeys = ['label', 'title', 'name'];

    foreach ($labelKeys as $labelKey) {
      if (isset($this->originalData[$labelKey])) {
        return $this->originalData[$labelKey];
      }
    }

    return NULL;
  }

  /**
   * Returns the langcode of this translation.
   *
   * @return string
   *   Language unique identifier.
   */
  public function langcode() : string {
    return $this->langcode;
  }

  /**
   * Get a single value from the original data.
   *
   * @param string $fieldName
   *   The field name key.
   *
   * @return string|array
   *   A single value the list of values.
   */
  public function get($fieldName) {
    return $this->originalData[$fieldName] ?? NULL;
  }

}
