<?php

namespace Drupal\blacksmith;

use Drupal;
use Drupal\blacksmith\Exception\BlacksmithInvalidItemConfiguration;

/**
 * Class BlacksmithItem.
 *
 * @package Drupal\blacksmith
 */
class BlacksmithItem {

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Blacksmith item identifier.
   *
   * @var string
   */
  protected $id;

  /**
   * ID of the file in which the item can be found.
   *
   * @var string
   */
  protected $group;

  /**
   * Data taken from the Yaml file.
   *
   * @var array
   */
  protected $originalData;

  /**
   * Translation in each languages.
   *
   * @var \Drupal\blacksmith\BlacksmithTranslation[]
   */
  protected $translations;

  /**
   * BlacksmithItem constructor.
   *
   * @param array $data
   *   Data from the Blacksmith source file.
   * @param string $groupId
   *   The Blacksmith group ID the item is part of.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithInvalidItemConfiguration
   */
  public function __construct(array $data, $groupId) {
    $this->validateData($data);
    $this->id = $data['id'];
    unset($data['id']);
    $this->group = $groupId;
    $this->originalData = $data;
    $this->setTranslations();
    // @todo Find a way to do dependency injection in this.
    $this->languageManager = Drupal::service('language_manager');
  }

  /**
   * Makes sure that the parameters given to the constructor are valid.
   *
   * @param array $data
   *   Data from the original YAML file.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithInvalidItemConfiguration
   */
  protected function validateData(array $data) : void {
    if (!isset($data['id'])) {
      throw new BlacksmithInvalidItemConfiguration("Missing parameter 'ID' in Blacksmith item.", $this);
    }

    if (!isset($data['entity_type'])) {
      throw new BlacksmithInvalidItemConfiguration("Missing parameter 'entity_type' in Blacksmith item.", $this);
    }

    if (!isset($data['bundle'])) {
      throw new BlacksmithInvalidItemConfiguration("Missing parameter 'bundle' in Blacksmith item.", $this);
    }
  }

  /**
   * Return the unique ID.
   *
   * @return string
   *   Blacksmith item's unique identifier.
   */
  public function id() : string {
    return $this->id;
  }

  /**
   * Return the selector that will be used in Blacksmith.
   *
   * @param string $separator
   *   The character used to separate the item's group and ID.
   *
   * @return string
   *   The item's selector.
   */
  public function selector($separator = '/') : string {
    return $this->group . $separator . $this->id;
  }

  /**
   * Get the item's group.
   *
   * @return string|null
   *   Item's group name.
   */
  public function group() : ?string {
    return $this->group ?? NULL;
  }

  /**
   * Gets the type of entity.
   *
   * @return string
   *   The entity type machine name.
   */
  public function entityType() : string {
    return $this->originalData['entity_type'];
  }

  /**
   * Get the item's label.
   *
   * @return string
   *   The bundle machine name.
   */
  public function label() : string {
    $labelKeys = ['label', 'title', 'name'];

    foreach ($labelKeys as $labelKey) {
      if (isset($this->originalData[$labelKey])) {
        return $this->originalData[$labelKey];
      }
    }

    return $this->selector('-');
  }

  /**
   * Get the item's bundle.
   *
   * @return string
   *   The bundle machine name.
   */
  public function bundle() : string {
    $bundleKeys = ['bundle', 'type'];

    foreach ($bundleKeys as $bundleKey) {
      if (isset($this->originalData[$bundleKey])) {
        return $this->originalData[$bundleKey];
      }
    }

    return '';
  }

  /**
   * Get a single value from the original data.
   *
   * @param string $fieldName
   *   The field name key.
   *
   * @return string|array
   *   A single value the list of values.
   */
  public function get($fieldName) {
    return $this->originalData[$fieldName] ?? NULL;
  }

  /**
   * Defines if a value is set for a specific field.
   *
   * @param string $fieldName
   *   The field name key.
   *
   * @return bool
   *   Whether or not a value has been set for this field.
   */
  public function hadField(string $fieldName) : bool {
    return isset($this->originalData[$fieldName]);
  }

  /**
   * Retrieves every translations available in the Blacksmith item.
   *
   * @return \Drupal\blacksmith\BlacksmithTranslation[]
   *   Each translation of the Blacksmith item.
   */
  public function getTranslations() : array {
    return $this->translations ?? [];
  }

  /**
   * Retrieves a specific translation available in the Blacksmith item.
   *
   * @param string $langcode
   *   The language code of the translation.
   *
   * @return \Drupal\blacksmith\BlacksmithTranslation|null
   *   Each translation of the Blacksmith item or NULL.
   */
  public function getTranslation($langcode) : ?BlacksmithTranslation {
    return $this->translations[$langcode] ?? NULL;
  }

  /**
   * Define the item translations based on the available data.
   *
   * @throws \Drupal\blacksmith\Exception\BlacksmithInvalidItemConfiguration
   */
  protected function setTranslations() : void {
    if (isset($this->originalData['_translations'])) {
      $translations = $this->originalData['_translations'];

      if (!is_array($translations)) {
        throw new BlacksmithInvalidItemConfiguration('The translations property is invalid.', $this);
      }

      // Make sure that the site support multilingualism.
      if (!$this->languageManager->isMultilingual()) {
        throw new BlacksmithInvalidItemConfiguration('The blacksmith item cannot declare translations if the website doesn\'t have multilingual support.', $this);
      }

      // Go through each translations.
      foreach ($translations as $langcode => $translationData) {
        if ($this->languageManager->getLanguage($langcode) === NULL) {
          throw new BlacksmithInvalidItemConfiguration('The item has a translation in a language that is not supported. (langcode : ' . $langcode . ')', $this);
        }

        // Add the translation to the list.
        $this->translations[$langcode] = new BlacksmithTranslation($langcode, $translationData);
      }

    }
  }

}
